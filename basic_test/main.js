var app = new Vue({
  el: '#app',
  data: {
    product: "Socks",
    image: "./assets/image.jpg",
    inStock: true,
    details: ["20% cat, 30% sad, all-tears"],
    variants: [
      {
        VariantId: 2234,
        VariantColor: "green",
        VariantImage: "./assets/green-socks.jpg"
      },
      {
        VariantId: 2235,
        VariantColor: "blue",
        VariantImage: "./assets/blue-socks.jpg"
      }
    ],
    cart: 0
  },
  methods: {
    addToCart() {
      this.cart+=1
    },
    updateProduct(VariantImage) {
      this.image = VariantImage
    }
  }
})
